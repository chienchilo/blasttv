#SAVe

A pipeline by using BLAST to do Taxonomy profiling result Validation.

NGS and taxonomic classifiers have been employed to help identify organisms within metagenomic samples. Most existing profiling tools are designed to be ultra sensitive, detecting all possible organisms in the sample. Despite various filters applied, the combination of incomplete sequence databases, similar sequences among genomes and sequencing errors and biases within different platforms still results in high false discovery rates (FDR) especially for organisms at trace levels. Here, we developed an alignment-based method to verify the profiling results by searching for complementary in extensive databases. Our tool will extract the classified reads and align them through a combination of NCBI non redundant nucleotide, WGS genomic and other databases possibly available. This reports a per-read BLAST-like report that only focuses on high scoring hits with good sequence coverage and an amalgamation of top hits to suggest if we think that the tool called the right organism. If the user desires SAVe can also create the files needed to make Krona plot (with the --krona flag) or the files nessary to make an pie chart in R. The software is developed in Perl and is compatible with any unix-like operating system.

* Version: 0.1

##Installing BlastTV
Download the latest version of BlastTV from [bitbucket](https://bitbucket.org/chienchilo/blasttv/downloads/) or use `git clone` from command line.

```
git clone https://chienchilo@bitbucket.org/chienchilo/blasttv.git
```

## Hardware Requirements

Due to the involvement of big database (nt), and NCBI Blast+ software supports multiple threads.

Minimum requirement recommended: 16GB memory and 8 computing CPUs.

In terms of Disk space, the NT database is around 57GB (20170701) and WGS db is an optional which may double the NT database size.

##Dependencies
BlastTV run requires following dependencies which should be in your path.

### Programming/Scripting languages
- [Perl >=v5.18.2](https://www.perl.org/get.html)
    - The pipeline has only been tested in v5.18.2

### Unix
- sort

### Third party softwares/packages
- [Blast+ (2.5.0)](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download)

### Database configuration
- [Taxonomy: taxdb](ftp://ftp.ncbi.nih.gov/blast/db/): need setup the BLASTDB environment variable to contain the path to the installation directory of the taxdb.
- [NT](ftp://ftp.ncbi.nlm.nih.gov/blast/db/)

(Optional)
- [REFSEQ] (ftp://ftp.ncbi.nlm.nih.gov/blast/db/)
- [WGS](ftp://ftp.ncbi.nlm.nih.gov/blast/WGS_TOOLS/README_BLASTWGS.txt)

## Running BlastTV

```
Usage:
    SAVe --in FASTQ_FILE or FASTA_FILE --target "TARGET ORGANISM" --nt /path/to/nt/database/nt --altDB --alt_db_location /path/to/alt/db/database_name --word_size word_size --num_threads number_of_threads -dust dust_filter ("yes" or "no") --perc_identity percent_identity_cutoff --pie --krona
Or see:
    SAVe --help for detailed usage

```

## Test

```
cd test
../SAVe --in 510_024_Haemophilus_parainfluenzae.fastq --target "Haemophilus parainfluenzae" 

```

## Outputs
```
Determination on whether the target organism is in the sample: Yes, No, or Maybe
Number of reads: number
Number of reads that called the target organism: number
Number of reads that called a different organism or several organisms that were not the target: number
Number of reads that called several organisms including the target (ambiguous reads): number

Different organisms called:
Organism Name

#Reads organized by reads that didn't call target, ambiguous reads, then reads that called the target
Read Name
Organism Name: Score

```
- Scoring info:
    - Scoring is based on the blast bitscore
    - The program:
        - Takes the best bitscore per organism
        - Divides it by the sum of the bitscores for all organisms in that read
        - Classifies each read into one of three groups: 'Yes', 'No', or 'Maybe' based on the above score.
           - If the organism is present and has a score of greater than .5, the read gets a 'yes'.
           - If the organism is not present or a different organism has a score of greater than .5 it gets a 'no'.
           - If the organism is present but has a score less than or equal to .5 and there is no organism with a score greater than .5 the read gets a 'maybe'.
        - The group with the majority of the reads is the answer given.
        

- If the user wants to create a krona plot and/or pie chart with this program, the user can also specify the option --krona and/or --pie to create files that will be used to make them.
- For more options type
```
SAVe --help
```
## Contact Info

- Challacombe, Zoe Merced: <zchallacombe@lanl.gov>
- Chien-Chi Lo: <chienchi@lanl.gov>

## Copyright

Copyright (2017).  Triad National Security, LLC. All rights reserved.
 
This program was produced under U.S. Government contract 89233218CNA000001 for Los Alamos National 
Laboratory (LANL), which is operated by Triad National Security, LLC for the U.S. Department of Energy/National 
Nuclear Security Administration.
 
All rights in the program are reserved by Triad National Security, LLC, and the U.S. Department of Energy/National 
Nuclear Security Administration. The Government is granted for itself and others acting on its behalf a nonexclusive, 
paid-up, irrevocable worldwide license in this material to reproduce, prepare derivative works, distribute copies to 
the public, perform publicly and display publicly, and to permit others to do so.

This is open source software; you can redistribute it and/or modify it under the terms of the GPLv3 License. If software 
is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with 
the version available from LANL. Full text of the [GPLv3 License](https://bitbucket.org/chienchilo/blasttv/src/b2d22634ef36/LICENSE) can be found in the License file in the main development 
branch of the repository.