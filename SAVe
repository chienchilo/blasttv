#!/usr/bin/perl
use File::Copy;
use File::Path;
use Cwd;
use warnings;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
my ($query, $target) = '';
my $dbYes = 0;
my $nt = "/panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt";
my $altDB = "/opt/database/refseq_genomic/refseq_genomic";
$wordsize = 24;

$filter = 'no';
$percid = 90;
$targetseqs = 50;
$pie = 0;
$krona = 0;
my $outfile = getcwd;
#get command line options
GetOptions (
	'help|?!' => \$opt_help,
	'in|i=s' => \$query,
        'target|t=s' => \$target,
	'altDB+' => \$dbYes,
	'nt=s' => \$nt,
	'alt_db_location=s' => \$altDB,
	'num_threads=i' => \$numthreads,
	'word_size|wd=i' => \$wordsize,
	'max_target_seqs|mts=i' => \$targetseqs,
	'dust|d=s' => \$filter,
	'perc_identity|pid=i' => \$percid,
	'pie+' => \$pie,
	'krona+' => \$krona,
	'path=s' => \$outfile,
) || pod2usage(2);

#set blast options
if($numthreads){
	$blastOptions = "-word_size $wordsize -num_threads=$numthreads -max_target_seqs $targetseqs -dust $filter -perc_identity $percid";
}else{
	$blastOptions = "-word_size $wordsize -max_target_seqs $targetseqs -dust $filter -perc_identity $percid";
}
pod2usage(-verbose => 2) && exit if defined $opt_help;

my $current = getcwd;
my $start = time;

my $file_end;

if($query =~ m/.fastq/){
        $file_end = ".fastq";
}elsif($query =~ m/.fasta/){
	$file_end = ".fasta";
}
#ensure that the input can be processed
print "Please input either a fasta or fastq file.\n" unless defined $file_end;
pod2usage(-verbose => 1) && exit unless defined $file_end;

#Make directory where files will live
my $path = $outfile."/save_output";
mkpath ($path, {
	mode => 0755,
});
#output file names
my $file_start = join('_', (split /\s/, $target));
my $fastq = (split /\//, $query)[-1];
(my $report = $fastq) =~s/$file_end/.out/;
(my $ntFile = $fastq) =~ s/$file_end/.nt/;                                                                                                                                                                                                                                   
(my $altDBFi = $fastq) =~ s/$file_end/.altdb/;
my $details = $file_start."_blast.details.txt";
my $ambig_orgs_file = $file_start."_ambiguous_orgs.txt";
my $pie_file = $file_start."_pie_chart_file.txt";
my $not_target_fi = $file_start."_not_target_species.txt";
my $plot_file = $file_start."_krona_plot.out.tab_tree";


#Fastq to fasta: also ensures that two reads with the same name arent getting combined because thats a problem
if($file_end eq ".fastq"){
	($fa = $query) =~ s/.fastq/.fasta/; 
        $fa = "$path/$fa";
	$readno = 0;
	open (my $FH, '<', $query) or die "Can't open $query: $!";
	open (my $fh, '>', $fa) or die "Can't open file $fa: $!";

	while (<$FH>){
		my $h1 = $_;
		my $s  = <$FH>;
		my $h2 = <$FH>;
		my $q  = <$FH>;
		@checkID = (split, substr($h1, 1));
		$readname = $checkID[0];
		if(exists $reads{$readname}){
			$readname = $readname."!@";
		}
		$reads{$readname} = 0;
		$h1 = $readname." ".$checkID[1]."\n";
		die "ERROR: expected '@' but saw $h1" if substr($h1, 0, 1) ne '@';
		$readno ++;
		print $fh ">", substr($h1,1);
		print $fh $s;
	}
	close $fh or die "Couldn't close file $fa: $!\n";
	close $FH or die "Couldn't close $query: $!\n";
}elsif($file_end eq ".fasta"){
	$fa = $query;
	$readno = 0;
        open (my $FH, '<', $query) or die "Can't open $query: $!";

        while (<$FH>){
                $readno ++;
        }
        close $FH or die "Couldn't close $query: $!\n";
}
print "BLASTing, this may take a while.\n";
#Blast query file against nt and wgs bacteria
#print `/opt/bin/blastn -query $fa -db $nt -word_size 24 -max_target_seqs 50 -dust no -outfmt "6 qseqid sseqid pident length qcovs qstart qend evalue bitscore sscinames staxid" -out $path/$ntFile`;
print `blastn -query $fa -db $nt $blastOptions -outfmt "6 qseqid sseqid pident length qcovs qstart qend evalue bitscore sscinames staxid" -out $path/$ntFile`;
print "Done querying the nt database.\n";
$duration = time - $start;
printf "Duration: %d hours, %d minutes and %d seconds\n",(gmtime $duration)[2,1,0];

if ($dbYes){
print `blastn -query $fa -db $altDB $blastOptions -outfmt "6 qseqid sseqid pident length qcovs qstart qend evalue bitscore sscinames staxid" -out $path/$altDBFi`;
#print `/opt/bin/blastn -query $fa -db $wgs -word_size 24 -max_target_seqs 50 -dust no -outfmt "6 qseqid sseqid pident length qcovs qstart qend evalue bitscore sscinames staxid" -out $path/$wgsFile`;
$duration = time - $start - $duration;
print "Done querying the wgs database, beginning sort proccess.\n";
printf "Duration: %d hours, %d minutes and %d seconds\n",(gmtime $duration)[2,1,0];
}

#find all the species of each file based on the tax id
sub cutTax{
	$blastFi = shift;
	chdir $path;
	open(my $fh, '<', $blastFi) or die "Couldn't open file $blastFi:$!";
	while($r = <$fh>){
	        my ($taxa, $species) = (split/\t/, $r)[10,9];
        	chomp ($taxa);
        	chomp ($species);
		$species =~ s/\w+\s\w+\s\K.+//;
		$species =~ s/\s+$//;
        	$tax{$taxa} = $species;
	}
	close $fh or die "Couldn't close $blastFi:$!";
}
#Change directory and split the blast output file into individual files by read
chdir $path;


my %fileNames;
my %try;
my @files;
my $i = 0;

#subroutine to splite each file into its individual reads
sub splitFiles {
	my $tmpFile = shift;
	chdir $path;
	open(my $F, '<', "$tmpFile") or die "Couldn't open file $tmpFile: $!";
	chomp(my @lines = <$F>);
	foreach my $j(@lines){
		my ($readFileName) = (split(" ", $j))[0];
		$try{"@".$readFileName} = 0;
		$readFileName =~ tr{/}{~};
		#$files[$i] = $readFileName;
		open (my $ofh, '>>', "$readFileName") or die "Couldn't open file $readFileName: $!";
		print $ofh "$j\n";
		close $ofh or die "Couldn't close file $readFileName: $!";
	}
    	close $F or die "Couldn't close file $tmpFile: $!";
}
&cutTax($ntFile);
&cutTax($wgsFile) if $dbYes;

&splitFiles($ntFile);
&splitFiles($wgsFile) if $dbYes;

#=start
chdir $current;
#Make an array of the previously split read files
opendir(DIRHANDLE, "$path") || die "Cannot opendir $path: $!";
my @files = grep {!/^\.+$/ && !/.txt/ && !/$file_start/ && !/.out/ && !/.nt/ && !/.altdb/ && !/.tab_tree/ && !/.fasta/ && !/.fastq/ && !/.html/} readdir(DIRHANDLE);
closedir(DIRHANDLE);
chdir $path;
#=end
#=cut
#Bitscore cutoff
open(my $infh, '>', "$report") or die "Couldn't open file $report: $!";

my $i = 0;

foreach my $tmp (@files){
        open(my $ifh, '<', "$tmp") or die "Couldn't open file $tmp: $!";
	#sort file
	$sort = `sort -n -rk 9,9 $tmp -o $tmp`;
        my @lines = <$ifh>;
	#max bitscore;
	my $max = (split /\t/, $lines[0])[8];
        my $percent = 0;
        my $goodtmp = "good".$tmp;
	#the "good file" is one that has no bitscore drop of greater than 10% of max bitscore
       	$goodfiles[$i] = $goodtmp;
	open (my $fg, '>>', "$goodtmp") or die "Couldn't open $goodtmp";
	#for every line, the difference between the current score and the max bitscore 
	#cannot be greater than 10% of max bitscore
        foreach my $l (@lines){
                $score = (split /\t/, $l)[8];
                $percent = ($max - $score)/$max;
		#print "$l\n$score, $percent\n";
		print $fg $l unless $percent >= .10;
        }
	$i ++;
        close $ifh or die "Couldn't close file $tmp: $!";
        close $fg or die "Couldn't close $goodtmp:$!";
}

my %ambiguousOtherOrgs;

#find the files that match the target
foreach my $g (@goodfiles){
	undef %scores;
	my $read = substr ($g, 4);
	$total = 0;
	$score = 0;
	open($F, '<', "$g") or die "Couldn't open $g: $!";
	while ($line = <$F>){
		my ($taxid, $score) = (split /\t/, $line) [10, 8];
		chomp ($taxid);
		my $spec = $tax{$taxid};
		if($spec =~ /N\/A/){
			$spec = "Scientific name not available. Taxid $taxid";
		}
		$scores{$spec} = $score unless exists $scores{$spec};
	}
	foreach my $species (keys %scores){
		$total += $scores{$species};	
		$reads{$read}{$species} = $total;
	}
	$match = 0;
        $notmatch = 0;
        $both = 0;
	foreach my $s (sort{ $scores{$b} <=> $scores{$a} } keys %scores){
		$finalScore = $scores{$s};
		$avg = $finalScore/$total;
		if($s =~ m/\b$target/ && $avg > .5){
			$match = 1;
		}elsif($s !~ m/\b$target/ && $avg > .5){
			$notmatch = 1;
			#$nomatch{$s} = $avg;
			$not_targetRead{$s} += 1;
		}elsif(exists $reads{$read}{$target} && $avg <= .5 ){
			$both = 1;
		}else{
                        $notmatch = 1;
                } 
		if($match == 1){
                        $goodReads{$read}{$s} = $avg;
                }elsif ($notmatch == 1){
                        $badReads{$read}{$s} = $avg;
                }elsif($both == 1){
                        $ambig{$read}{$s} = $avg;
			$ambiguousOtherOrgs{$s} += 1;
		}
		
		
	}
}
#numbers that match the target vs. those that dont or are ambigious
$match = scalar (keys %goodReads);
$notmatch = scalar (keys %badReads);
$both = scalar (keys %ambig);
if($match > $notmatch && $match > $both){
	print $infh "$target is in the sample\n";
}elsif($notmatch > $match && $notmatch > $both){
	print $infh "$target is NOT in the sample\n";
}elsif($both > $match && $both > $notmatch){
	print $infh "$target could be in the sample\n"; 

}
print $infh "Number of reads: $readno\n";
print $infh "Number of reads assigned to target organism: $match\n";
print $infh "Number of reads assigned to a different organism: $notmatch\n";
print $infh "Number of ambiguous reads: $both\n\n";
print $infh "Different organisms:\n";
 
open(my $pfh, '>', $plot_file) or die "Couldn't open $plot_file: $!\n";

foreach my $notspec (keys %not_targetRead){
        print $infh "$notspec: $not_targetRead{$notspec}\n";
	print $pfh $not_targetRead{$notspec}."\tNot Target\t$notspec\n";
}

#print reads to the report file
foreach my $read (keys %badReads){
	print $infh "$read\n";
	foreach my $name (sort { $badReads{$read}{$b} <=> $badReads{$read}{$a} } keys %{ $badReads{$read} }) {
		print $infh "$name: ";
		printf $infh "%.3f\n", $badReads{$read}{$name};
	}
	print $infh "\n\n";
}

foreach my $read (keys %ambig){
        print $infh "$read\n";
	foreach my $name (sort { $ambig{$read}{$b} <=> $ambig{$read}{$a} } keys %{ $ambig{$read} }) {
                print $infh "$name: ";
                printf $infh "%.3f\n", $ambig{$read}{$name};
	}
        print $infh "\n\n";
}

foreach my $read (keys %goodReads){
	print $infh "$read\n";
        foreach my $name (sort { $goodReads{$read}{$b} <=> $goodReads{$read}{$a} } keys %{ $goodReads{$read} }) {
                print $infh "$name: ";
                printf $infh "%.3f\n", $goodReads{$read}{$name};
	}
	print $infh "\n\n";
}

close $infh or die "Couldn't close file $report: $!";

open(my $infh, '<', "$report") or die "Couldn't open file $report: $!";
my $line_no = 0;
#print from the report file to the files needed to make the pie chart
if($pie){
	open (my $rfh, '>', "$pie_file") or die "Couldn't open file $pie_file: $!";
        open (my $RFH, '>', "$not_target_fi") or die "Couldn't open file $not_target_fi: $!";
        open (my $afh, '>', "$ambig_orgs_file") or die "couldnt open $ambig_orgs_file: $!";
	while($line = <$infh>){
		if($line_no < 5){
			print $rfh $line;
		}elsif($line_no > 5 && $line ne "\n"){
			print $RFH $line;
		}elsif($line_no > 5 && $line eq "\n"){
			last;
		}
		$line_no ++;
	}
	foreach my $key(keys %ambiguousOtherOrgs){
		print $afh "$key: $ambiguousOtherOrgs{$key}\n";
	}
	close $rfh or die "Couldn't close file $pie_file: $!";
	close $RFH or die "Couldn't close file $not_target_fi: $!";
	close $afh or die "Couldn't close file $ambig_orgs_file:$!";
}

#print from the report file to make the krona plot file
if($krona){
	open(my $pfh, '>', "$plot_file") or die "Couldn't open $plot_file: $!\n";
	foreach my $key(keys %ambiguousOtherOrgs){
                print $pfh $ambiguousOtherOrgs{$key}."\tAmbiguous\t$key\n";
        }
	print $pfh $match."\tTarget Organism\t$target\n";
	print $pfh ($readno - $match - $notmatch - $both)."\tNo hit\n";
	close $pfh or die "Couldn't close file $plot_file: $!";
}
close $infh or die "Couldn't close file $report: $!";


open (my $BFH, '>', "$details") or die "Couldn't open $details: $!";
print $BFH "Sequence ID; GI/GB Identifier; Percent Identity; Length; Query Coverage; Query Start; Query End; E-value; Bitscore; Scientific Name; Taxid;\n";
foreach my $goodtmp (@goodfiles){
	open (my $g, '<', "$goodtmp") or die "Couldn't open $goodtmp: $!";
	my @lines = <$g>;
	foreach my $l (@lines){
		print $BFH $l;
	}
	close $g or die "Couldn't close $goodtmp: $!";
	print $BFH "\n";
}

close $BFH or die "Couldn't close file $details: $!";
#=end
#=cut


chdir $current;
opendir(DIRHANDLE, "$path") || die "Cannot opendir $path: $!";
my @files = grep {!/^\.+$/ && !/.txt/ && !/$file_start/ && !/.out/ && !/.nt/ && !/.altdb/ && !/.tab_tree/ && !/.fasta/ && !/.fastq/ && !/.html/} readdir(DIRHANDLE);
closedir(DIRHANDLE);
chdir $path;
foreach my $tmp (@files){
        unlink $tmp;
}

if($pie != 0){
	unlink $plot_file;
}
if($krona != 0){
	my $krona_fi = $file_start.".krona.html";
	print $plot_file;
	`ktImportText $path/$plot_file -o $path/$krona_fi`;
	unlink $pie_file;
	unlink $ambig_orgs_file;
	unlink $not_target_fi;
}

=head1 NAME

BlastAutoPercent - gives a "confidence rating" for EDGE outputs based on BLAST results of read.

=head1 SYNOPSIS

BlastAutoPercent
	--in=/path/to/file.fastq 
	--target="Target organism" 
	--nt /path/to/database/database 
	--altDB 
	--alt_db_location /path/to/database/database 
	--word_size integer 
	--num_threads integer  
	--max_target_seqs integer
	--dust "yes" or "no"
	--perc_identity integer
	--pie
	--krona

=head1 OPTIONS
B<--in, -i>	The input file in fastq format.

B<--target, -t>	The target organism that you require verification of.

B<--wgsDB>	Specify if you want to BLAST against another database as well as nt.

B<--nt>	Specify the directory where nt is located, default is "/opt/database/nt/nt"

B<--wgs>	Specify the directory where the other database is located

B<--word_size, -wd>	Blast word size, default is 24

B<--num_threads>	Specify number of worker threads to create

B<--dust, -d>	Blast's low complexity filter, default is off. Specify with "yes" or "no"

B<--percent_identity, -pid>	Blast percent identity cutoff, default is 90%

B<--pie>	Create files to be used to make several pie charts

B<--krona>	Create a file to be used in making a krona plot

B<--help, -h>	Help message.

=head1 DESCRIPTION

This program is used to validate calls made by the EDGE pipeline about the presenece of a species in a
sample.

=cut


