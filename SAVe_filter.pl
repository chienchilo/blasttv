#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use File::Path;
#number of reads to keep
my $cutoff = 500;

my @validateFiles;
my $pathogenFi;
my $outFolder='~/';
my $krona;
my $pie;
my $help = 0;
GetOptions(
	'help|?' => \$help,
        'cutoff|c=s' => \$cutoff,
	'files|f=s' => \@validateFiles,
        'pathogen|p=s' => \$pathogenFi,
	'outfolder=s' => \$outFolder,
	'krona+' => \$krona,
	'pie+' => \$pie,
)||pod2usage(1);

pod2usage(-verbose => 2) && exit if $help;
my %pathogens;


sub getOrganisms{
	my %organisms;
        my $orgFile = shift;
	open (my $fh, '<', $orgFile) or die "Couldn't open $orgFile: $!";
	
	while(my $r = <$fh>){
		chomp ($r);
		my ($level, $organism, $readNo) = (split/\t/, $r)[0,1,8];
		if($level eq "species"){
			if($pathogenFi){
				if(exists $pathogens{$organism}){
					if($readNo < $cutoff){
						$pathogens{$organism} = $readNo;
					}
				}
			}else{
				if($readNo < $cutoff){
                                        $organisms{$organism} = $readNo;
                                }
			}
		}
	}
        close $fh or die "Couldn't close $orgFile: $!";
	if($pathogenFi){
		return %pathogens;
	}else{
		return %organisms;
	}

}

sub filterByPathogen{
        open(my $FH, '<', $pathogenFi) or die "Couldn't open $pathogenFi: $!";
        my $header = <$FH>;
        while(my $l = <$FH>){
		my $pathogen = (split/\t/, $l)[0];
		$pathogens{$pathogen} = 0;

        }
        close $FH or die "Couldn't close $pathogenFi: $!";
}
#=start
foreach my $file (@validateFiles){
	my @pathway = (split/\//, $file);
        my $fileName = pop @pathway;
	my $bamFile = $fileName;
        $bamFile =~ s/.list.txt/.bam/;
        $bamFile = join("/", @pathway, $bamFile);
	my $fastq = $fileName;
        my @splitFileName = (split/-/, $fileName);
        my $tool = $splitFileName[1];
	if($pathogenFi){
		&filterByPathogen();
		foreach my $key(keys %pathogens){
		        my %toBeValidated = &getOrganisms($file);
			if($toBeValidated{$key} < $cutoff && $toBeValidated{$key} > 0){
				my @taxonomyClassification = (split/\s/, $key);
				my $name = $tool."_".$taxonomyClassification[0]."_".$taxonomyClassification[1]."_".(split/\./, $splitFileName[3])[0];
				#print ("$key, $pathogens{$key}\n");
				my $fastq = $name.".se.fastq";
				`/panfs/biopan01/edge-dev-master/edge-dev-test/scripts/microbial_profiling/script/bam_to_fastq_by_taxa.pl -rank species -mapped -name "$key" -prefix $outFolder/$name $bamFile`;
				if($krona && $pie){
					`SAVe -in $fastq -target "$key" -nt /panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt -krona -pie -path $outFolder`;
			
				}elsif($pie){
                                        `SAVe -in $fastq -target "$key" -nt /panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt -pie -path $outFolder`;

				}elsif($krona){
                                        `SAVe -in $fastq -target "$key" -nt /panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt -krona -path $outFolder`;
				}
			}
		}
	}else{
		my %toBeValidated = &getOrganisms($file);
                foreach my $key(keys %toBeValidated){
                        if($toBeValidated{$key} < $cutoff){
                                my @taxonomyClassification = (split/\s/, $key);
                                my $name = $tool."_".$taxonomyClassification[0]."_".$taxonomyClassification[1]."_".(split/\./, $splitFileName[3])[0];
                                my $fastq = $name.".se.fastq";
                                `/panfs/biopan01/edge-dev-master/edge-dev-test/scripts/microbial_profiling/script/bam_to_fastq_by_taxa.pl -rank species -mapped -name "$key" -prefix $outFolder/$name $bamFile`;
                	
                                if($krona && $pie){
                                        `SAVe -in $fastq -target "$key" -nt /panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt -krona -pie -path $outFolder`;
                        
                                }elsif($pie){
                                        `SAVe -in $fastq -target "$key" -nt /panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt -pie -path $outFolder`;

                                }elsif($krona){
                                        `SAVe -in $fastq -target "$key" -nt /panfs/biopan01/edge-dev-master/edge-dev-test/database/nt/nt -krona -path $outFolder`;
                                }
			} 
		}
        }
                                                                       
}
#=end
#=cut
=head1 NAME

Does prefiltering before running SAVe

=head1 SYNOPSIS

SAVe_filter.pl
	--cutoff integer less than 1000
	--files files that save will validate
	--pathogen the pathogen.txt file if pathogens only will be validated
	--outfolder the folder to store all output - default is working directory
	--krona make a krona plot
	--pie make files to make a pie chart

=head1 OPTIONS

B<--cutoff, -c>	Any organism called with reads less than this will be verified by save

B<--files>	Files separated by a space for save to validate

B<--pathogen>	SAVe will only validate pathogens

B<--outfolder>	Output folder

B<--help, -h>   Help message.

B<--krona>	Krona plot

B<--pie>	Files for pie plot
=head1 DESCRIPTION

This program preprocesses edge data to ensure speedy SAVe results.

=cut

