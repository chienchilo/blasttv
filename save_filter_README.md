#SAVe_filter

Prefilters save results by multiple options to minimize the time it takes save to run
Using this script save can analyze all of the organisms in edge results under a certain read cutoff. The user can also choose whether or not to only validate pathogens, or to validate all organisms.

## usage

```
SAVe_filter.pl
        --cutoff integer less than 1000 (default 500)
        --files files that save will validate
        --pathogen the pathogen.txt file if pathogens only will be validated
        --outfolder the folder to store all output (default is working directory)
        --krona make a krona plot
        --pie make files to make a pie chart
```
#Example:
```
SAVe_filter.pl
	--cutoff 200 --files file1 --files file2 -outfolder -krona
```